import __sharedModule from './__sharedModule';
$(document).ready(function() {
  //Declaración de constantes
  const unitPrice = $("#price");
  const frigorias = $("#frigorias");
  const relatedProducts = $("#related-products");
  const carousel = $("#wrap-carousel");
  const carouselPpal = $("#wrap-carousel-ppal");
  var price;
  //Carga de selector de productos e imagenes
  sharedModule.getProductList().then(data => {

    $.each(data, function(i, result) {

      frigorias.append("<option value=" + result.id + ">" + result.description + "</option>");
    })
    sharedModule.getCarousel(frigorias.val()).then(data => {
      //append de imágenes
      var i = 1;
      do {
        carousel.append(`
          <div class="wrap-galeria">
            <img src="${data[i].url}" class="img-responsive" alt="">
          </div>
            `)
        i++;
      }
      while (i < data.length);
      carouselPpal.append(`
        <img src="${data[0].url}" class="img-responsive" alt="">
      `);
    });
    price = data[0].unitPriceInCents;
    unitPrice.append(`<h3>$${price}</h3>`);
    $('#units').attr('max', data[0].minQuantity);
    $('#units').change(function() {
      sharedModule.getPriceByUnits(price, $(this).val()).then((result) => {
        unitPrice.empty();
        unitPrice.append(`<h3>$${result}</h3>`);
      })
    })
  });
  //Carga de imagenes según select
  frigorias.change(function() {
    unitPrice.empty();
    var id = $(this).val();
    sharedModule.getProductList().then(data => {
      var priceNew = data.filter((result) => result.id === id)[0];
      unitPrice.append(`<h3>$${priceNew.unitPriceInCents}</h3>`);

      $('#units').attr('max', priceNew.minQuantity);
      $('#units').val(1);
      $('#units').change(function() {
        sharedModule.getPriceByUnits(price, $(this).val()).then((result) => {
          unitPrice.empty();
          unitPrice.append(`<h3>$${result}</h3>`);
        })
      })
    });
    carousel.empty();
    carouselPpal.empty();

    sharedModule.getCarousel(id).then(data => {
      var i = 1;
      do {
        carousel.append(`
          <div class="wrap-galeria">
            <img src="${data[i].url}" class="img-responsive" alt="">
          </div>
            `)
        i++;
      }
      while (i < data.length);
      carouselPpal.append(`
        <img src="${data[0].url}" class="img-responsive" alt="">
      `);
    });

  });
  //Calculo precio por unidades

  // Carga productos relacionados
  sharedModule.getRelatedProducts().then(data => {
    $.each(data, function(i, result) {
      relatedProducts.append(`<div class="col-xs-12 col-sm-4 col-md-4">
        <div class="ifix-product-box  shadow-box">
          <a href="#!" class=""><img class="img-responsive"w src='${result.pictureUrl}' alt="Climatización"></a>
          <div>
            <div class="content-desc text-center">
              <h5>${result.title}</h5>
              <span class="price"><i class="fa fa-credit-card" aria-hidden="true"></i> desde $${result.fromPrice}</span>
              <p>${result.description}</p>
              <div class="text-center">
                <a href="#!" class="btn btn-secondary btn-out-rosa" role="button">Contratar</a>
              </div>
            </div>
          </div>
        </div>
      </div>`);
    })
  });
});
