$(document).ready(function() {
  var sharedModule;
  const url = "https://private-70cb45-aobara.apiary-mock.com/";
  var frigorias = $("#frigorias");
  sharedModule = {

    getProductList: function() {
      return $.getJSON(url + "product/list", function(data) {
          return data;
        })
        .done(function() {
          console.log("second success");
        })
        .fail(function() {
          console.log("error");
        })
    },
    getCarousel: function(id) {
      var id = id;
      return $.getJSON(url + "product/" + id + "/photos", function(data) {
          return data;
        })
        .done(function() {
          console.log("second success");
        })
        .fail(function() {
          console.log("error");
        })
    },
    getRelatedProducts: function() {
      return $.getJSON(url + "related-product/list", function(data) {
          return data;
        })
        .done(function() {
          console.log("second success");
        })
        .fail(function() {
          console.log("error");
        })
    },
    getPriceByUnits: function(price, units) {
      return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(price * units);
        }, 100);
      })
    }
  };

  window.sharedModule = sharedModule;
});
