# IguanaFix Test
## Webpack 3 + SASS + Bootstrap 3.3.7 + Font Awesome
Proyecto realizado para IguanaFix como test challenge frontend.
[Criterios de aceptación](https://docs.google.com/document/d/1xr8Wm0GOsK55jONtT9W-BXgIXWakRrVxOQo_zm30v9o/edit)

Usando namespace objects para encapsular funciones (Module Pattern)

<br>
## DEMO

[IguanaFix Test](http://iguanafix.surge.sh/)
<br>
## Install dependencies

```
npm install
```


## Develop locally with webpack-dev-server
1. Run

```
npm run dev
```

2. In your browser, navigate to: [http://localhost:2000/](http://localhost:2000/)
## For bundled output

```
npm run build
```

## For production-ready output

```
npm run build:prod
```

## Loaders and Plugins used in this boilerplate

### Loaders
* babel-loader
* html-loader
* sass-loader
* css-loader
* style-loader
* file-loader

### Plugins
* clean-webpack-plugin
* extract-text-webpack-plugin
* html-webpack-plugin
